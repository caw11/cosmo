#ifndef _COSMO_PARAMS_H
#define _COSMO_PARAMS_H

// Cosmology parameters (for cosmology class)
double s8(0.83); // rms mass fluctuation on 8/hlittle Mpc scales at z=0
double h(0.67); // little hubble h
double om0(0.315); // at z=0
double lam0(0.685); // at z=0
double omb(0.049); // at z=0
double omNu(0.0);
double n(0.96); /* power law index for the power spectrum */

#endif
